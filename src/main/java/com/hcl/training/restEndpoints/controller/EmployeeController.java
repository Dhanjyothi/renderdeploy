package com.hcl.training.restEndpoints.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.training.restEndpoints.model.Employee;
import com.hcl.training.restEndpoints.service.EmployeeServiceIntf;

@RestController
@RequestMapping("/employee")

public class EmployeeController {
	@Autowired
	EmployeeServiceIntf empService;

	@PostMapping(value = "/saveData")
	public String saveEmployeeData(@RequestBody Employee emp) {
		empService.saveData(emp);
		return "Saved Succesfully";
	}
	@DeleteMapping(value="/deleteData/{id}")
	public ResponseEntity<String> deleteEmployee(@PathVariable("id")int id)
	{
		if(empService.findById(id).isPresent())
		{
			empService.deleteData(id);
		
		return new ResponseEntity<>("deleted successfully",HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity<>("Failed to delete",HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping(value="/fetchAllData")
	public List<Employee> fetchAllEmployees()
	{
		List<Employee> ls =empService.fetchAllEmployees();
		return ls;
		
	}
	@GetMapping(value="/fetchByName/{name}")
	public List<Employee> fetchByName(@PathVariable("name") String name)
	{
		List<Employee> ls =empService.fetchByName(name);
		return ls;
		
	}
	
}