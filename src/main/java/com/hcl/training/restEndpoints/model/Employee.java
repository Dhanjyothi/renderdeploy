package com.hcl.training.restEndpoints.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter 
@Setter
@Entity
@Table(name="employeedetails") 
public class Employee {
	@Id
	@GeneratedValue(generator="increment")
	@ApiModelProperty(required = false, hidden = true)
	private Integer employeeId;
	
	private String employeeName;
	
	private String address;
	
	private String emailId;
	
	private String contactNumber;

	private String password;

}
